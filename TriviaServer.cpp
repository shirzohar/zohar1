#include "TriviaServer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>       
#include <thread>
#include <vector>
#include <mutex>          // std::mutex, std::unique_lock, std::defer_lock

std::mutex mtx;           // mutex for critical section


using namespace std;
TriviaServer::TriviaServer()
{
	/* The port number is passed as an argument */
	
	try
	{
		// create a socket
		// socket(int domain, int type, int protocol)
		_socket = socket(AF_INET, SOCK_STREAM, 0);
		if (_socket < 0)
			throw "ERROR opening socket";
	}
	catch (char* msg)
	{
			perror(msg);
	}
}
TriviaServer::~TriviaServer()
{

	closesocket(TriviaServer::_socket);


}

void TriviaServer::TriviaServer::serve()
{

	bindAndListen();
	thread Thread(&TriviaServer::clientHandler, this);
	Thread.detach();
	while (true)
	{
		TRACE ("sccepting client");
		try
		{
			accept();
		}
		catch (...)
		{

		}
	}



}

void TriviaServer::bindAndListen()
{
	int iResult;
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr("127.0.0.1");
	clientService.sin_port = htons(27015);
	try
	{
		iResult = ::bind(TriviaServer::_socket, (SOCKADDR *)& clientService, sizeof(clientService));
		if (iResult == SOCKET_ERROR)
		{
			throw (L"bind function failed with error: %ld\n", WSAGetLastError());
		}
	}
	catch (char* msg)
	{
		cout << msg << endl;
	}
	try
	{
		if (::listen(TriviaServer::_socket, SOMAXCONN) == SOCKET_ERROR)
			throw (L"listen function failed with error: %d\n", WSAGetLastError());
		cout << (L"Listening on socket...\n") << endl;
	}
	catch (char* msg)
	{
		cout << msg << endl;
	}
}

void TriviaServer::accept()
{
	SOCKET socket_client = ::accept(TriviaServer::_socket, NULL, NULL);
	if (socket_client== INVALID_SOCKET)
	{
		throw("ERROR!invalid socket!!");
	}
	thread accept_thread(&TriviaServer::_socket,this,socket_client);
	accept_thread.detach();	
}
void TriviaServer::clientHandler(SOCKET socket_client)
{

	RecievedMessage* rm;
	int Bmessage;
	do
	{
		Bmessage == Helper::getMessageTypeCode(socket_client);
		string message = to_string(Bmessage);
		int length = message.length();
		if ((message[length - 1] != 0) || (message[0] != 299))
		{
				rm = buildRecieveMessage(socket_client, Bmessage);
				addRecievedMessage(rm);
		}
	}
	while (Bmessage=!0);
	RecievedMessage* quit;
	quit= buildRecieveMessage(socket_client, QUIT_APP);
	_queRcvMessages.push(quit);

}
RecievedMessage* buildRecieveMessage(SOCKET client_socket, int Bmessage)
{

		int message = (Bmessage);
		RecievedMessage _RM(client_socket, Bmessage);
		vector<string> values;
		if (message == ASK_SIGN_IN)
		{
			//////////����� ����� ����� �� ����
		}

}
Room* TriviaServer::getRoomById(int roomId)
{
	Room* room;
	auto search = _roomsList.find(roomId);
	if (search != _roomsList.end()) {
		room = search->second;
		}
		else {
			room = NULL;
		}
		
			return room;
}
User* TriviaServer::getUserByName(string username)
{
	User* user;
	User* GoodUser;
	std::map<SOCKET, User*>::iterator it_type;
	for (it_type = _connectedUsers.begin(); it_type != _connectedUsers.end(); it_type++){
		user = it_type->second;
		if (user->getUsername == username) 
		{
			GoodUser = user;
		}
		else 
		{
			GoodUser = NULL;
		}
	}

	return GoodUser;
}

User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	User* user;
	User* GoodUser;
	std::map<SOCKET, User*>::iterator it_type;
	for (it_type = _connectedUsers.begin(); it_type != _connectedUsers.end(); it_type++){
		user = it_type->second;
		if (user->getSocket == client_socket)
		{
			GoodUser = user;
		}
		else
		{
			GoodUser = NULL;
		}
	}

	return GoodUser;
}
void TriviaServer:: handleRecievedMessages()
{
	RecievedMessage* chargeMessage;
	SOCKET userSock;
	User* theUser;
	int message;
	vector <string> messageValuse;
	unique_lock<mutex> lck(mtx, defer_lock);
	if (_queRcvMessages.empty())
	{
		_CV.wait(lck);
		chargeMessage = _queRcvMessages.back();
		_CV.notify_all();
		userSock = chargeMessage->getSock();
		theUser = getUserBySocket(userSock);//get the user
		message = chargeMessage->getMesaageCode();//get the code
		if (message == ASK_SIGN_IN)
		{
			theUser = handleSignin(chargeMessage);

		}

		else if(message == ASK_SIGN_OUT)
		{
			handleSignout(chargeMessage);
		}

		else if(message == ASK_SIGN_UP)
		{
			bool succeses = handleSignup(chargeMessage);
		}


		else if(message == ASK_ROOM_LIST)
		{
			handleGetRoom(chargeMessage);
		}


		else if(message == ASK_ROOM_USERS)
		{
			handleGetUsersInRoom(chargeMessage);
		}


		else if(message == ASK_JOIN_ROOM)
		{
			bool succeses = handleJoinRoom(chargeMessage);
		}



		else if(message == ASK_CREATE_ROOM)
		{
			bool succeses = handleCreateRoom(chargeMessage);
		}
		else if(message == ASK_CLOSE_ROOM)
		{

			bool succeses = handleCloseRoom(chargeMessage);
		}

		else if(message == ASK_START_GAME)
		{
			handleStartGame(chargeMessage);
		}

		else if(message == C_SEND_ANS)
		{
			handlePlayerAnswer(chargeMessage);
		}
		else if(message == LEAVE_GAME)
		{
			handleLeaveGame(chargeMessage);
		}

		else if(message == ASK_BEST_SCORES)
		{
			handleGetBestScores(chargeMessage);
		}

		else if(message == ASK_PERSONSL_SITUATION)
		{
			handleGetPersonalStatus(chargeMessage);
		}
		else if (message == QUIT_APP)
		{
			safeDeleteUser(chargeMessage);
		}
		else
		{
			safeDeleteUser(chargeMessage);
		}
		
		_queRcvMessages.pop();
	}


}
void TriviaServer::safeDeledUser(RecievedMessage* _RM)
{
	SOCKET clientSock;
	clientSock = _RM->getSock;
	handleSignout(_RM);
	//////////////////////
}
	
User* TriviaServer::handleSignin(RecievedMessage* _RM)
{
	vector <string> values;
	values = _RM->getValues();
	string user;
	string password;
	user = values.front();
	password = values.back();
	bool exists;
	exists = _DB->isUserAndPassMatch(user, password);//check if the user exists
	if (exists == false)
	{

	}
}
